$(document).ready(function(){
	
	var lastClickedItem = null;


	var editing = false;

	var lastClickedItemIndex = 0;
	var clickInDay = false;
	
	var parkingLength = 0; /* Number of elements in park (only DEMO) */
	var newParkingLength = 0;

	
	
	var day1 = model.addDay();
	//var activity1 = new Activity();
	
	createTestData(); // For testing
	
	
	// Show time allocation for different activities
	function allocateTime() {
		$('.time-allocation').empty(); // Empty the box, so that we can fill it with the new stats
		
		var totalLength = day1.getTotalLength(); // get total time by which we divide the categoryTime
		for (var i = 1; i <= 4; i++) {
			var categoryTime = day1.getLengthByType(i);
			var categoryPercent = parseInt((categoryTime/totalLength)*100);
			
			console.log("allocation:"+i+": "+categoryPercent);
			
			// Fill the box:
			$('.time-allocation').append('<div data-type="'+i+'" style="height:'+categoryPercent+'px"></div>');
		}
	}
	
	
	
	// Get items from DOM list
	function getItems(listId) {
		var columns = [];

		$('.sortable-list#'+listId+' li').each(function(){
			columns.push($(this).attr('data-name')); // get Name of list item			
		});

		return columns;
	}
	
	// Example 1.2: Sortable and connectable lists
	$('#example-1-2 .sortable-list').sortable({
		connectWith: '#example-1-2 .sortable-list'
	});
	$('#example-1-2 .sortable-list#day-list').sortable({
		update: function(){
			
			if(clickInDay) { // Moved an element inside Day list
				
				// Moving away or just sorting?
				newParkingLength = $('.column.left.first ul li').length;
				console.log("New parking length: "+newParkingLength);
				
				
				if(parkingLength != newParkingLength) { // If number of elements in parking has changed (i.e. if something has been dragged back)
					// Remove activity
					day1._removeActivity(lastClickedItemIndex);
				}
			}
			else {
				// Todo! Also check if length of elements in day has changed (because only sorting in parking is also possible) => in that case: do bottom code
				day1._addActivity(lastClickedItem);
			}
			
			
		    console.log(getItems('day-list'));
		    //console.log("length: "+day1.getTotalLength());
		    $('#totalDayLength').html(day1.getTotalLength());
	
		    // Get time data from input
			var inputTime = $('#example-1-2 .column input').val();
			
			inputHours = parseInt(inputTime.substr(0,2));
			inputMin = parseInt(inputTime.substr(3,4));
			

			console.log(inputHours+"   "+inputMin);

			// Create new start time
			day1.setStart(inputHours, inputMin);
			dayStartTime = parseInt(day1._start);
			
			console.log("Total day length: "+day1.getTotalLength()); // Testing

			console.log('Day length: '+lastClickedItem.getLength());


			numberOfActivities = parseInt($("#day-list li").length);
			// Change time-format in day div			
			$('.left #day-list li').each(function(index, li){
				activityStartInMin = dayStartTime;
				activityIndex = $(this).index();
				console.log("TJENsadadas2: "+activityIndex);
				//if (numberOfActivities>1) {
				for (var i = 1; i<=activityIndex; i++) {
					activityStartInMin = activityStartInMin + parseInt($('.left #day-list li:nth-child('+i+')').attr('data-duration'));
					console.log("TJENFEN: "+$('.left #day-list li:nth-child('+i+')').attr('data-duration'));
					console.log("TJEN12: "+activityStartInMin);
				};
				
				console.log($(this).attr('data-name') + parseInt($(this).attr('data-duration')));
				activityStartTime = (('0' + String((Math.floor(activityStartInMin/60)))).slice(-2) + ":" +  ('0' + String(('0' + activityStartInMin % 60))).slice(-2));
				$(this).find('.sortable-item-time').html(activityStartTime);				
			});

			
			
			// Show element from parked list that we clicked
			console.log(lastClickedItem);
			
			console.log("LAST CLICKED: "+lastClickedItem.getName());
			

			// Show end time
			$('#endTime').html(day1.getEnd());
			
			
			console.log(day1);
			
			allocateTime();
			
			
			// Get name of first activity
			//console.log(day1._activities[0].getName());
	    }
	});

	// Sets the start and end time
	$('.column input[type=time]').click(function(){
		// Get time data from input
			var inputTime = $('#example-1-2 .column input').val();
			
			inputHours = parseInt(inputTime.substr(0,2));
			inputMin = parseInt(inputTime.substr(3,4));

			console.log(inputHours+"   "+inputMin);

			// Create new start time
			day1.setStart(inputHours, inputMin);

			$('#endTime').html(day1.getEnd());
	});
	

	// Create a new activity
	$('#add-activity').click(function(){
		$('body').addClass('show-popup');
		$('#editActi').show();

	});

	$('#add-day').click(function(){

		$('#example-1-2').append('<div class="column left"><div class="time-allocation"></div><p id="startTime">\
			</p><input type="time" value="07:00"/><p>End time: <span id="endTime">07:00</span></p>\
			<p>Total length: <span id="totalDayLength">0</span> min</p>\
			<ul class="sortable-list" id="day-list"></ul>\
			</div>');

		$('#example-1-2 .sortable-list').sortable({
			connectWith: '#example-1-2 .sortable-list'
		});
	});
	
	
	$('.sortable-list li').live('mousedown', function() {
		
		console.log("mousedown");
		
		var index = $(this).index();
	
	    
	    var listContainer = $(this).closest('.column');
	    
	    // If click in Parked Activities box (adding to days)
	    if(listContainer.hasClass('first')) {
	    	console.log("IN PARKED");
		    clickInDay = false;
		    lastClickedItem = model.parkedActivities[index];
	    }
	    else {
	    	console.log("IN DAY");
		    clickInDay = true;
		    lastClickedItem = day1._activities[index];
	    }
	    
	    lastClickedItemIndex = index; // Save index of clicked element
	    
	    parkingLength = $('.column.left.first ul li').length;
	    console.log("Parking length: "+parkingLength);
	    
	    //console.log(lastClickedItem);
	
	});
	
	// Edit an activity
	$('.sortable-list').bind('click', 'li', function(event){

		console.log($(this).index());

		$('#editActi .name').val(lastClickedItem.getName());
		$('#editActi .length').val(lastClickedItem.getLength());
		$('#editActi .type').val(lastClickedItem.getTypeId());
		$('#editActi .description').val(lastClickedItem.getDescription());

		editing = true;

		$('body').addClass('show-popup');
		$('#editActi').show();


	});

	// Save or discard new activity
	$('#save-activity, #cancel-activity').click(function(){
		
		// Get id of clicked element ("save" or "cancel"?)
		var type = $(this).attr('id');
		
		// Get data from inputs
		var inputName = $('#editActi .name');
		var inputLength = $('#editActi .length');
		var inputType = $('#editActi .type');
		var inputDescription = $('#editActi .description');
		
		// Create new activity object (for model)
		var newActivity = new Activity(inputName.val(),inputLength.val(),inputType.val(),inputDescription.val());
		

		// if click on save
		if(type == "save-activity") {

			if (editing) {

				lastClickedItem.setName(inputName.val());
				lastClickedItem.setLength(inputLength.val());
				lastClickedItem.setTypeId(inputType.val());
				lastClickedItem.setDescription(inputDescription.val());

				/* $('.column.left.first ul li').append('<li class="sortable-item" data-name="'+newActivity.getName()+'"><span class="sortable-item-time">'+newActivity.getLength()+' min</span><span class="sortable-item-name" data-type="'+newActivity.getTypeId()+'">'+newActivity.getName()+'</span></li>'); */
			}

			else{				
				// Add object to parked activities
				model.addActivity(newActivity);
				
				//console.log(newActivity.getName());
				
				// Format HTML with newly created activity and append it to list ----- data-type="'+newActivity.getTypeId()+'"
				$('.column.left.first ul').append('<li class="sortable-item" data-duration="'+newActivity.getLength()+'" data-name="'+newActivity.getName()+'"><span class="sortable-item-time">'+newActivity.getLength()+' min</span><span class="sortable-item-name" data-type="'+newActivity.getTypeId()+'">'+newActivity.getName()+'</span></li>');
			}
		}
		
		// Empty all inputs
		inputName.val('');
		inputLength.val('');
		inputType.val('0');
		inputDescription.val('');

		editing = false;
			
		// hide popup
		$('body').removeClass('show-popup');
		$('#editActi').hide();

	});

});